FROM mcr.microsoft.com/dotnet/core/sdk:8.0 AS build-env
WORKDIR /app
COPY *.csproj ./
RUN dotnet restore
COPY . ./
RUN dotnet publish -c Release -o output
# Runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:8.0
WORKDIR /app
COPY --from=build-env /app/output .
ENTRYPOINT ["dotnet", "Student.dll"]